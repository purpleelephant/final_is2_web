<?php

namespace App\Http\Controllers;
use App\EmpDept;
use App\Last;
use App\Salaries;
use DB;

use Illuminate\Http\Request;
use App\Employees;
use App\Departments;
use App\Titles;

class EmployeeController extends Controller
{



	public function index(Request $request){


        return view('layouts.index');

    }
    public function hired(Request $request){
        $first = Employees::where('hire_date', '<', '1987-01-01')->count();
        $sec = Employees::whereBetween('hire_date', ['1987-01-01', '1989-01-01'])->count();
        $third = Employees::whereBetween('hire_date', ['1989-01-01', '1991-01-01'])->count();
        $fourth = Employees::whereBetween('hire_date', ['1991-01-01', '1993-01-01'])->count();
        $fifth = Employees::whereBetween('hire_date', ['1993-01-01', '1995-01-01'])->count();
        $six = Employees::whereBetween('hire_date', ['1995-01-01', '1997-01-01'])->count();
        $seven = Employees::whereBetween('hire_date', ['1997-01-01', '1999-01-01'])->count();
        $eight = Employees::whereBetween('hire_date', ['1999-01-01', '2000-01-28'])->count();
        $arrayOfEmps =[$first, $sec, $third, $fourth, $fifth, $six, $seven, $eight];

        return view('layouts.hired', compact('arrayOfEmps'));

    }
    public function fired(Request $request){
        $first = Last::where('to_date', '<', '1987-01-01')->count();
        $sec = Last::whereBetween('to_date', ['1987-01-01', '1989-01-01'])->count();
        $third = Last::whereBetween('to_date', ['1989-01-01', '1991-01-01'])->count();
        $fourth = Last::whereBetween('to_date', ['1991-01-01', '1993-01-01'])->count();
        $fifth = Last::whereBetween('to_date', ['1993-01-01', '1995-01-01'])->count();
        $six = Last::whereBetween('to_date', ['1995-01-01', '1997-01-01'])->count();
        $seven = Last::whereBetween('to_date', ['1997-01-01', '1999-01-01'])->count();
        $eight = Last::whereBetween('to_date', ['1999-01-01', '2001-01-28'])->count();
        $nine = Last::whereBetween('to_date', ['2001-01-01', '9999-01-01'])->where('to_date', '<>', '9999-01-01')->count();
        $ten = Last::where('to_date', '=', '9999-01-01')->count();

        $arrayOfEmps =[$first, $sec, $third, $fourth, $fifth, $six, $seven, $eight, $nine, $ten];

        return view('layouts.fired', compact('arrayOfEmps'));

    }
    public function averageSalary(Request $request){
       $eng = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'engineer')->avg('salaries.salary');
       $staff = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'staff')->avg('salaries.salary');
       $seniorEng = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'Senior Engineer')->avg('salaries.salary');
       $seniorStaff = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'Senior staff')->avg('salaries.salary');
       $assisEng = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'Assistant Engineer')->avg('salaries.salary');
       $techLead = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'Technique Leader')->avg('salaries.salary');
       $manag = Employees::join('salaries', 'employees.emp_no', '=', 'salaries.emp_no')->join('titles', 'employees.emp_no', '=', 'titles.emp_no')->where('titles.title', '=', 'manager')->avg('salaries.salary');

       $arrayOfEmps =[$eng, $assisEng, $seniorEng, $staff, $seniorStaff, $techLead, $manag];

        return view('layouts.averageSalary', compact('arrayOfEmps'));

    }
    public function empInfo($empno){

        $emp = Employees::find($empno);
        $salary = Salaries::where('emp_no', '=', $empno)->get();
        $dep = Departments::join('dept_emp','departments.dept_no','=','dept_emp.dept_no')->join('employees','dept_emp.emp_no','=','employees.emp_no')->select('departments.dept_name','dept_emp.from_date','dept_emp.to_date')->where('employees.emp_no',$empno)->get() ;
        $title = Titles::where('emp_no', '=', $empno)->get();
        $man = Employees::join('dept_manager','employees.emp_no','=','dept_manager.emp_no')->join('departments','dept_manager.dept_no','=','departments.dept_no')->select('dept_manager.*')->where('dept_manager.emp_no',$empno)->get();
     	if($man->count() == 1)
     		$manOrNot='Yes';
     	else
     		$manOrNot='No';

        return view('layouts.info', compact('emp','salary','dep','title','manOrNot'));

    }

     public function board(Request $request, $depart=null){
     	$depsort=  $request->department;

     	if($depsort=='/' || $depsort==null){
	        $emps=(new Employees)->paginate(10);
	        $deps=Departments::all();

	        return view('layouts.dashboard', compact('emps','deps'));
    	}

    	$emps=(new Employees)->join('dept_emp','employees.emp_no','=','dept_emp.emp_no')->join('departments','dept_emp.dept_no','=','departments.dept_no')->select('employees.*')->where('departments.dept_name',$depsort)->paginate(10);
    	$deps=Departments::all();

        $emps->setPath('/dashboard?department='.$depsort);

    	return view('layouts.dashboard', compact('emps','deps','depsort'));


    }

    public function search(Request $request){
	    $value = $request->search;
	    $line = preg_split('/\s+/', $value);
	    //dd($line);

	    if(sizeof($line)==2) {
	        $emps = Employees::where('first_name', 'LIKE', $line[0] . '%')->where('last_name', 'LIKE', $line[1] . '%')->orWhere('last_name', 'LIKE', $line[0] . '%')->where('first_name', 'LIKE', $line[1] . '%')->paginate(10);
	    }
	    else
	        $emps = Employees::where('emp_no', 'LIKE', $line[0] . '%')->orWhere('first_name', 'LIKE', $line[0] . '%')->orWhere('last_name', 'LIKE', $line[0] . '%')->paginate(10);

	    if($emps==null){
	            echo "Sorry, nothing was found";
	    }

            //$emps=Employees::where('first_name', 'LIKE',$value.'%')->orWhere('last_name', 'LIKE',$value.'%')->orWhere('emp_no', 'LIKE',$value.'%')->paginate(10);
        $deps=Departments::all();


        return view('layouts.dashboard', compact('emps', 'deps', 'value'));
    }

    public function graphs(Request $request){

	    $dnine = EmpDept::where('dept_no', '=', 'd009')->count();
        $deight = EmpDept::where('dept_no', '=', 'd008')->count();
        $dseven = EmpDept::where('dept_no', '=', 'd007')->count();
        $dsix = EmpDept::where('dept_no', '=', 'd006')->count();
        $dfive = EmpDept::where('dept_no', '=', 'd005')->count();
        $dfour = EmpDept::where('dept_no', '=', 'd004')->count();
        $dthree = EmpDept::where('dept_no', '=', 'd003')->count();
        $dtwo = EmpDept::where('dept_no', '=', 'd002')->count();
        $done = EmpDept::where('dept_no', '=', 'd001')->count();
        $arrayOfEmps =[$done, $dtwo, $dthree, $dfour, $dfive, $dsix, $dseven, $deight, $dnine];


        return view('layouts.graphboard', compact('arrayOfEmps'));

    }
    public function salary($id){
	    $emps = Employees::find($id);
	    $salaries = Salaries::find($id);
	    

	    return view('emp', compact('emps', 'salaries'));

    }

}
