<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $primaryKey = 'emp_no';
   	protected $table='employees';

    public function titles(){

    	return $this->hasMany('App\Titles','emp_no','emp_no');
    }

    public function departmentss()
    {
        return $this->belongsToMany('App\Departments','dept_emp');
    }
}
