<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titles extends Model
{

	protected $table='titles';


    public function employees(){

    	return $this->belongsTo('App\Employees','emp_no');

    }
}
