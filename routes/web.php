<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','EmployeeController@index')->name('index');

Route::get('/dashboard/{depart?}', 'EmployeeController@board')->name('dashboard');
Route::post('/dashboard/{search?}', 'EmployeeController@search')->name('search');
Route::get('/employee/{empno?}', 'EmployeeController@empInfo')->name('empInfo');
Route::get('/graphboard','EmployeeController@graphs')->name('graphs');
Route::get('/hired','EmployeeController@hired')->name('hired');
Route::get('/fired','EmployeeController@fired')->name('fired');
Route::get('/averageSalaries','EmployeeController@averageSalary')->name('averageSalary');
// Route::get('/', 'CrudController@index')->name('crud.index');


// Route::get('crud/create', 'CrudController@create')->name('crud.create');

// Route::post('crud/store', 'CrudController@store')->name('crud.store');

// Route::get('crud/{id}/edit', 'CrudController@edit')->name('crud.edit');

// Route::post('crud/{id}/update', 'CrudController@update')->name('crud.update');

// Route::delete('crud/{id}/destroy', 'CrudController@destroy')->name('crud.destroy');

// Route::get('crud/show', 'CrudController@show')->name('crud.show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


