@extends('layout')


    
@section('content')
        <div class="container">
            
            <h3>My list</h3>
            <a href="{{route('crud.create')}}" class="btn btn-success">Create</a>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Title</td>
                                <td>Description</td>
                                <td>Actions</td>

                            </tr>
                        </thead>
                    @foreach($crud as $task)
                    <tbody>
                        <tr>
                            <td>{{ $task->id }}</td>
                            <td>{{ $task->title }}</td>
                            <td>{{ $task->description}}</td>
                            <td>
                                
                                <a href="" class="" >
                                    <i class="glyphicon glyphicon-eye-open"></i>  
                                </a>
                                <a href="{{ route('crud.edit', $task->id) }}" class="" >
                                    <i class="glyphicon glyphicon-edit"></i>  
                                </a>
                                {!! Form::open(['method' => 'DELETE','route' => ['crud.destroy', $task->id]]) !!}
                                <button onclick="return confirm('Are you sure?')">
                                    <i class="gpyphicon glyphicon-remove"></i>
                                </button>
                                {!! Form::close() !!}


                            </td>        
                        </tr>
                    </tbody>  
                  @endforeach                 
                </table>
                </div>   
            </div>
            
        </div>

@endsection