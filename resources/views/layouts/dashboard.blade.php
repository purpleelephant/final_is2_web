
@extends('layout')

@section('content')


    <!-- Portfolio Grid Section -->
    <section class="portfolio" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
             <form  method="POST" action="{{route('search')}}">
                {{ csrf_field() }}
                <div class="row col-xs-12">

                    <div class="col-xs-11">
                      <input class="form-control" id="searchit1"  type="search" name="search" placeholder=" First name, Last name">
                    </div>
                    <div class="col-xs-1">
                      <button class="btn btn-sm btn-success">SEARCH</button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
          <div class="panel panel-primary filterable">

              <div class="panel-heading">
                <div class="row">
                  <h3 class="panel-title col-xs-4">Employees</h3>

                        <form class="col-xs-8" method="GET" action="{{route('dashboard')}}">
                          <div class="row">
                                    <div class="col-xs-6">
                                      <select class="form-control input-sm" name="department" id="department">
                                           <option value ="{{$depsort or '/'}}">{{$depsort or 'All departments'}}</option>

                                            @foreach($deps as $dep)

                                              <option value="{{$dep->dept_name}}">{{$dep->dept_name}}</option>
                                            @endforeach
                                      </select>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                      <button class="btn btn-sm btn-success">SUBMIT</button>
                                    </div>
                         
                          </div>
                        </form>
                    
                       
                      


                  
                </div>
              </div>
              

              <table class="table" style="text-align: center;">
                  <thead >
                      <tr class="filters" >
                          <th><input type="text" class="form-control" placeholder="ID" disabled style="text-align: center;"></th>
                          <th><input type="text" class="form-control" placeholder="First Name" disabled style="text-align: center;"></th>
                          <th><input type="text" class="form-control" placeholder="Last Name" disabled style="text-align: center;"></th>
                          <th><input type="text" class="form-control" placeholder="Gender" disabled style="text-align: center;"></th>
                          <th><input type="text" class="form-control" placeholder="Title" disabled style="text-align: center;"></th>
                          <th><input type="text" class="form-control" placeholder="More" disabled style="text-align: center;"></th>
                      </tr>
                  </thead>
                  

                  @foreach($emps as $emp)
                    <tbody>
                        <tr>
                            <td>{{ $emp->emp_no }}</td>
                            <td>{{ $emp->first_name }}</td>
                            <td>{{ $emp->last_name }}</td>
                            <td>{{ $emp->gender }}</td>

                            <td>{{$emp->titles->first()->title}}</td>
                            <td><a href="{{route('empInfo', $emp->emp_no)}}" > SHOW</a></td>
                        </tr>
                    </tbody>  
                  @endforeach     
              </table>
              <div class="text-center">
                {{ $emps->links() }}
              </div>
          </div>
          </div>
        </div>


      </div>
    </section>

@endsection