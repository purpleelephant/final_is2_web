@extends('layout')

@section('content')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>


	<div class="container">
		<div class="row">
			<div class="col-xs-3">
				<img src="{{asset('img/profile.png')}}" alt="">
				<div class="" style="border-radius: 10px; color: white; height: 118px; background-color: #337ab7; ">
					<h1 class="text-center" style="padding-top: 40px" >
						@if($emp->gender == 'M' )
							Male
						@else
                        	Female
                    	@endif

					</h1>
				</div>
			</div>

			<div class="col-xs-9 bg-primary" style="border-radius: 10px; font-size: 17px">
				<div class="row">
					<div class="col-xs-6">	
		        		<h1>{{ $emp->first_name }} {{ $emp->last_name }} </h1> 
		        	</div>
	        		<div class="col-xs-4"></div>
	        	</div>
        		<hr>
        		<p><strong>Birth Date:</strong> {{$emp->birth_date}}</p>
        		<p><strong>Hire Date:</strong> {{$emp->hire_date}}</p>
        		

        			@foreach($dep as $d)
        				@if($d->to_date=='9999-01-01')

                        <p><strong>Current department: </strong>{{$d->dept_name}}</p>

                    @else
                        <p><strong>Previous Department(s):</strong> <br>
        				from {{$d->from_date}} to {{$d->to_date}} , {{$d->dept_name}}<br></p>
                     @endif
        			@endforeach

                @foreach($title as $tit)
                    @if($tit->to_date=='9999-01-01')

                        <p><strong>Current position: </strong>{{$tit->title}}</p>

                    @else
                        <p><strong>Previous position: </strong> <br>
                            from {{$tit->from_date}} to {{$tit->to_date}} , {{$tit->title}}<br>
                    @endif
                @endforeach


        		<p><strong>Manager:</strong> 
					{{$manOrNot}}
        		</p>
        		<p><strong>Current Salary:</strong> {{$salary->last()->salary}} $</p>
        	</div>
        </div>
    </div>
    <br>
    <br>
    <br>
            <div class="container"  >

                <canvas id="myChart"></canvas>
            </div>
            <script>
                var ctx = document.getElementById("myChart").getContext('2d');
                var a = {!! json_encode($salary) !!};

                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: [@foreach($salary as $sal) {{$sal->from_date}}, @endforeach],
                        datasets: [{
                            label: '{{ $emp->first_name }} {{ $emp->last_name }}: salary history',

                            data:[@foreach($salary as $sal) {{$sal->salary}}, @endforeach],

                            backgroundColor: [
                                'rgba(25, 99, 132, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',

                            ],
                            borderWidth: 1

                        }]
                    },

                    options: {

                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            </script>



		

            

    <br>
   

@endsection