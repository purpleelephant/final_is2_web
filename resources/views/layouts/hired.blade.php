@extends('layout')

@section ('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

<div class="container">
    <h4 style="text-align: center;">Number of employees, that were hired</h4>
<canvas id="myChart"></canvas>
<br>

</div>
<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var a = {!! json_encode($arrayOfEmps) !!};
    var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
    labels: ["1985", "1987", "1989", "1991", "1993", "1995", "1997", "1999", "now"],
    datasets: [{
    label: 'Number of employees, that were hired',
    data: [0 ,a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)',
    'rgba(24, 159, 64, 0.2)',
    'rgba(24, 15, 44, 0.2)',
    'rgba(234, 19, 188, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(24, 159, 64, 1)',
    'rgba(24, 15, 44, 1)',
    'rgba(234, 19, 188, 1)'
    ],
    borderWidth: 1

    }]
    },

    options: {

    scales: {
    yAxes: [{
    ticks: {
    beginAtZero:true
    }
    }]
    }
    }
    });
</script>

<br>

@endsection
